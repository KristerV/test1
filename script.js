async function getChatData() {
    const messages = await fetchGET('https://app.koodikool.ee/sdb/chatapp-messages')
    for (const msg of messages) {
        document.querySelector('#answer').innerHTML += msg.message
    }
}
getChatData()
